Bakery is project DeeGee. An operating system in the new sense of the words.
Rather than having the operating system taking care of the hardware and
scheduling everything, it is an ecosystem of go projects, made to imitate
the usual operating system.

This adds a few things into DeeGee's power. No keeping up
with the current trends in hardware and if you can run Go on it,
it can be a DeeGee system.

At first everything will most likely be command line, and written by yours
truly(localtoast), but with time and testing, go projects will be written
by other people. Or not, I don't care. I'm still doing it. Using other libraries
will be done only with rigorous testing. Nothing is worse than thinking,
"Someone's already done this!" and having the thing collapse in a heap of
broken dreams and desire.

If anyone wants to jump out of the woodwork and say, "Hey! I'll help!"
that'd be great.

Good.

ok.

no takers?

FINE.
