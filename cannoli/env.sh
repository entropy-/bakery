export CGO_CPPFLAGS="-I/usr/local/include"
export CGO_LDFLAGS="-L/usr/local/lib -lopencv_shape -lopencv_videostab -lopencv_stitching -lopencv_photo -lopencv_ml -lopencv_dnn -lopencv_objdetect -lopencv_superres -lopencv_video -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_flann -lopencv_imgproc -lopencv_core"
export LD_LIBRARY_PATH=/usr/local/lib

